import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras import layers
import matplotlib.pyplot as plt
import optuna
import plotly

tf.random.set_seed(42)

(x_train, y_train), (x_test, y_test) = keras.datasets.fashion_mnist.load_data()

validation_split = 0.1
split_index = int(x_train.shape[0] * (1 - validation_split))
x_train, x_val = x_train[:split_index], x_train[split_index:]
y_train, y_val = y_train[:split_index], y_train[split_index:]

x_train = x_train.astype('float32') / 255.0
x_test = x_test.astype('float32') / 255.0

x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], x_train.shape[2], 1))
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], x_test.shape[2], 1))

y_train = keras.utils.to_categorical(y_train, num_classes=10)
y_test = keras.utils.to_categorical(y_test, num_classes=10)
y_val = keras.utils.to_categorical(y_val, num_classes=10)

labels = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
          'Scandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

plt.figure(figsize=(15, 15))
for i in range(25):
    plt.subplot(5, 5, i + 1)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(x_train[i].squeeze(), cmap=plt.cm.binary, interpolation='bicubic')
    plt.xlabel(labels[np.argmax(y_train[i])])
plt.show()


def objective(trial):
    num_layers = trial.suggest_categorical('num_layers', [3, 4])
    num_filters = trial.suggest_categorical('num_filters', [64, 128, 256])
    dropout_rate = trial.suggest_categorical('dropout_rate', [0.3, 0.4, 0.5])
    num_units = trial.suggest_categorical('num_units', [64, 128, 256, 512])
    activation = trial.suggest_categorical('activation', ['relu', 'sigmoid'])
    learning_rate = trial.suggest_categorical('learning_rate', [0.01, 0.1])

    model = keras.Sequential()

    model.add(layers.Conv2D(filters=num_filters, kernel_size=3, activation=activation, input_shape=(28, 28, 1),
                            padding='same'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    for _ in range(num_layers - 1):
        model.add(layers.Conv2D(filters=num_filters, kernel_size=3, activation=activation, padding='same'))
        model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Flatten())
    model.add(layers.Dropout(dropout_rate))

    for _ in range(num_layers):
        model.add(layers.Dense(num_units, activation=activation, kernel_initializer='glorot_uniform'))

    model.add(layers.Dense(10, activation='softmax'))

    optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit(x_train, y_train, validation_data=(x_val, y_val), epochs=10, batch_size=128)

    loss, accuracy = model.evaluate(x_test, y_test)

    trial.set_user_attr('model', model)
    return accuracy


study = optuna.create_study(direction='maximize')
study.optimize(objective, n_trials=50)

best_params = study.best_params
best_value = study.best_value

print('Best hyperparameters:', best_params)
print('Best accuracy:', best_value)

best_model = study.best_trial.user_attrs['model']
best_model.save('best_model.h5')

optuna.visualization.plot_optimization_history(study).show()
